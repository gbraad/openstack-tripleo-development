#!/bin/bash
# update projects when no git submodule is used
for i in $(find . -maxdepth 1 -type d)
do
    pushd $i
    git checkout master
    git pull --reb
    popd
done
